// import something here
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

// "async" is optional
export default async ({ Vue }) => {
  var firebaseConfig = {
    apiKey: "AIzaSyD_vALB8OY1aB8HC-E7FBQX68A2vVPiF9A",
    authDomain: "login-e2b5d.firebaseapp.com",
    databaseURL: "https://login-e2b5d.firebaseio.com",
    projectId: "login-e2b5d",
    storageBucket: "login-e2b5d.appspot.com",
    messagingSenderId: "702829321852",
    appId: "1:702829321852:web:631146095d1e43e76c33f9"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig)

  firebase.auth().languageCode = 'pt'

  Vue.prototype.$auth = firebase.auth
  Vue.prototype.$database = firebase.database
}